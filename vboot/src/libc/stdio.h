// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef VBOOT_SRC_LIBC_STDIO_H_
#define VBOOT_SRC_LIBC_STDIO_H_

int snprintf(char *buffer, size_t bufsz, const char *format, ...);

#endif  // VBOOT_SRC_LIBC_STDIO_H_
