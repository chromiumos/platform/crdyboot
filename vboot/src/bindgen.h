// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef VBOOT_SRC_BINDGEN_H_
#define VBOOT_SRC_BINDGEN_H_

// This is the public API for vboot.
#include <vb2_api.h>

// For CROS_CONFIG_SIZE and CROS_PARAMS_SIZE constants.
#include <futility/kernel_blob.h>

#endif  // VBOOT_SRC_BINDGEN_H_
