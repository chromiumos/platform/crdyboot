// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef VBOOT_SRC_LIBC_CTYPE_H_
#define VBOOT_SRC_LIBC_CTYPE_H_

// Intentionally empty.

#endif  // VBOOT_SRC_LIBC_CTYPE_H_
