// Copyright 2022 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef VBOOT_SRC_LIBC_STDLIB_H_
#define VBOOT_SRC_LIBC_STDLIB_H_

void *malloc(size_t);
void free(void *);

#endif  // VBOOT_SRC_LIBC_STDLIB_H_
