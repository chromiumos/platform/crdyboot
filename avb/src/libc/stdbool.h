// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef AVB_SRC_LIBC_STDBOOL_H_
#define AVB_SRC_LIBC_STDBOOL_H_

typedef _Bool bool;
#define true 1
#define false 0

#endif  // AVB_SRC_LIBC_STDBOOL_H_
