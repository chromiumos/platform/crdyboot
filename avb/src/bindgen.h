// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef AVB_SRC_BINDGEN_H_
#define AVB_SRC_BINDGEN_H_

#include <libavb/libavb.h>

#endif  // AVB_SRC_BINDGEN_H_
