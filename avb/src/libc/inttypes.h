// Copyright 2024 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef AVB_SRC_LIBC_INTTYPES_H_
#define AVB_SRC_LIBC_INTTYPES_H_

#define PRIu64 __UINT64_FMTu__
#define PRIx64 __UINT64_FMTx__

#endif  // AVB_SRC_LIBC_INTTYPES_H_
